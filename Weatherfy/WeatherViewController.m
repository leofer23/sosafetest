//
//  WeatherViewController.m
//  Weatherfy
//
//  Created by Eduardo Toledo on 12/13/17.
//  Copyright © 2017 SoSafe. All rights reserved.
//

#import "WeatherViewController.h"
#import "Weatherfy-Swift.h"
@import MapKit;

@interface WeatherViewController ()
@property (weak, nonatomic) IBOutlet MKMapView *mapView;
@property (weak, nonatomic) IBOutlet UITextField *textField;
@property (weak, nonatomic) IBOutlet UILabel *averageLabel;
@property (weak, nonatomic) IBOutlet UILabel *varianceLabel;
@property (strong, nonatomic) DataSource *dataSource;
@end

@implementation WeatherViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}

- (IBAction)onActionButtonPressed:(UIButton *)sender
{
    _dataSource = [[DataSource alloc] init];
    

    CLGeocoder *geoCoder = [[CLGeocoder alloc] init];
    NSString *address = _textField.text;
    
    [geoCoder geocodeAddressString:address completionHandler:^(NSArray *placemarks, NSError *error) {
        if(error){
            //do something
        }
        if(placemarks && placemarks.count > 0){
            CLPlacemark *placemark = placemarks[0];
            CLLocation *location =placemark.location;
            NSLog(@"lat:%f,lon:%f",location.coordinate.latitude,location.coordinate.longitude);
             [self moveMapToCoordinates:CLLocationCoordinate2DMake(location.coordinate.latitude, location.coordinate.longitude)];
            
               double average = [_dataSource meanWithData:_dataSource.data in:_textField.text];
            
            NSString* averageString = [@(average) stringValue];
            _averageLabel.text = averageString;
            
            double variance = [_dataSource varianceWithData:_dataSource.data in:_textField.text];
            
            NSString* varianceString = [@(variance) stringValue];
            _varianceLabel.text = varianceString;
            
            
        }
    }];
   
}

- (void)updateLabels
{
    self.averageLabel.text = @"-1";
    self.varianceLabel.text = @"-1";

}

- (void)moveMapToCoordinates:(CLLocationCoordinate2D)coordinates
{
    [self.mapView setCenterCoordinate:coordinates animated:YES];
}

@end
